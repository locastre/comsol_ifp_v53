function model = brain_mets_pipe(modelLabel, modelPath)
%
% simple_final.m
%
% Model exported on Nov 19 2019, 15:26 by COMSOL 5.3.0.260.

import com.comsol.model.*
import com.comsol.model.util.*

try 

model = ModelUtil.create('Model');

model.modelPath(modelPath);

model.label(modelLabel);

model.comments(['LCBM Nov 2019\n\n']);

load([modelPath filesep 'meanKtrans.mat'],'meanKtransPerSec');
if meanKtransPerSec == 0
    meanKtransPerSec = 0.0019;
end

model.param.set('meanKtrans', [num2str(meanKtransPerSec) ' [1/s]']);

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component.create('mcomp1', 'MeshComponent');

model.geom.create('mgeom1', 3);

model.file.create('res1');
model.file.create('res2');
model.file.create('res3');
model.file.create('res4');
model.file.create('res5');

model.component('comp1').func.create('int1', 'Interpolation');
model.component('comp1').func.create('int2', 'Interpolation');
model.component('comp1').func.create('int3', 'Interpolation');
model.component('comp1').func.create('int4', 'Interpolation');
model.component('comp1').func.create('int5', 'Interpolation');
model.component('comp1').func('int1').label('Ktrans');
model.component('comp1').func('int1').set('sourcetype', 'model');
model.component('comp1').func('int1').set('modelres', 'res1');
model.component('comp1').func('int1').set('importedname', 'Ktrans_persec.txt');
model.component('comp1').func('int1').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int1').set('importeddim', '3D');
model.component('comp1').func('int1').set('funcs', {'Ktrans' '1'});
model.component('comp1').func('int1').set('defvars', true);
model.component('comp1').func('int1').set('argunit', 'mm');
model.component('comp1').func('int1').set('fununit', '1/s');

model.file('res1').resource([modelPath filesep 'Ktrans_persec.txt']);

model.component('comp1').func('int1').set('source', 'file');
model.component('comp1').func('int1').set('nargs', '3');
model.component('comp1').func('int1').set('struct', 'spreadsheet');
model.component('comp1').func('int2').label('KH');
model.component('comp1').func('int2').set('sourcetype', 'model');
model.component('comp1').func('int2').set('modelres', 'res2');
model.component('comp1').func('int2').set('importedname', 'K_H.txt');
model.component('comp1').func('int2').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int2').set('importeddim', '3D');
model.component('comp1').func('int2').set('funcs', {'KH' '1'});
model.component('comp1').func('int2').set('defvars', true);
model.component('comp1').func('int2').set('argunit', 'mm');
model.component('comp1').func('int2').set('fununit', 'm^2/Pa/s');

model.file('res2').resource([modelPath filesep 'K_H.txt']);

model.component('comp1').func('int2').set('source', 'file');
model.component('comp1').func('int2').set('nargs', '3');
model.component('comp1').func('int2').set('struct', 'spreadsheet');
model.component('comp1').func('int3').label('Peff');
model.component('comp1').func('int3').set('sourcetype', 'model');
model.component('comp1').func('int3').set('modelres', 'res3');
model.component('comp1').func('int3').set('importedname', 'P_eff.txt');
model.component('comp1').func('int3').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int3').set('importeddim', '3D');
model.component('comp1').func('int3').set('funcs', {'Peff' '1'});
model.component('comp1').func('int3').set('defvars', true);
model.component('comp1').func('int3').set('argunit', 'mm');
model.component('comp1').func('int3').set('fununit', 'Pa');

model.file('res3').resource([modelPath filesep 'P_eff.txt']);

model.component('comp1').func('int3').set('source', 'file');
model.component('comp1').func('int3').set('nargs', '3');
model.component('comp1').func('int3').set('struct', 'spreadsheet');
model.component('comp1').func('int4').label('Lp');
model.component('comp1').func('int4').set('sourcetype', 'model');
model.component('comp1').func('int4').set('modelres', 'res4');
model.component('comp1').func('int4').set('importedname', 'L_p.txt');
model.component('comp1').func('int4').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int4').set('importeddim', '3D');
model.component('comp1').func('int4').set('funcs', {'Lp' '1'});
model.component('comp1').func('int4').set('defvars', true);
model.component('comp1').func('int4').set('argunit', 'mm');
model.component('comp1').func('int4').set('fununit', 'm/Pa/s');

model.file('res4').resource([modelPath filesep 'L_p.txt']);

model.component('comp1').func('int4').set('source', 'file');
model.component('comp1').func('int4').set('nargs', '3');
model.component('comp1').func('int4').set('struct', 'spreadsheet');
model.component('comp1').func('int5').label('SV');
model.component('comp1').func('int5').set('sourcetype', 'model');
model.component('comp1').func('int5').set('modelres', 'res5');
model.component('comp1').func('int5').set('importedname', 'S_V.txt');
model.component('comp1').func('int5').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int5').set('importeddim', '3D');
model.component('comp1').func('int5').set('funcs', {'SV' '1'});
model.component('comp1').func('int5').set('defvars', true);
model.component('comp1').func('int5').set('argunit', 'mm');
model.component('comp1').func('int5').set('fununit', '1/m');

model.file('res5').resource([modelPath filesep 'S_V.txt']);

model.component('comp1').func('int5').set('source', 'file');
model.component('comp1').func('int5').set('nargs', '3');
model.component('comp1').func('int5').set('struct', 'spreadsheet');

model.component('comp1').mesh.create('mesh1');
model.mesh.create('mpart1', 'mgeom1');
model.mesh('mpart1').create('imp1', 'Import');
model.mesh('mpart1').feature('imp1').set('source', 'stlvrml');
model.mesh('mpart1').feature('imp1').set('filename', [modelPath filesep 'iso-domain_ROI.stl']);
model.mesh('mpart1').run;

model.component('comp1').geom('geom1').lengthUnit('mm');
model.component('comp1').geom('geom1').create('imp1', 'Import');
model.component('comp1').geom('geom1').feature('imp1').label('Domain');
model.component('comp1').geom('geom1').feature('imp1').set('type', 'mesh');
model.component('comp1').geom('geom1').feature('imp1').set('mesh', 'mpart1');
model.component('comp1').geom('geom1').run;

model.component('comp1').physics.create('c', 'CoefficientFormPDE', 'geom1');
model.component('comp1').physics('c').field('dimensionless').field('p');
model.component('comp1').physics('c').field('dimensionless').component({'p'});
model.component('comp1').physics('c').create('dir1', 'DirichletBoundary', 2);
model.component('comp1').physics('c').feature('dir1').selection.set([1 2 3 4 5 6]);
model.component('comp1').physics('c').prop('Units').set('DependentVariableQuantity', 'pressure');
model.component('comp1').physics('c').prop('Units').set('SourceTermQuantity', 'fluidconductance');
model.component('comp1').physics('c').feature('cfeq1').set('c', {'comp1.KH' '0' '0' '0' 'comp1.KH' '0' '0' '0' 'comp1.KH'});
model.component('comp1').physics('c').feature('cfeq1').set('a', 'comp1.Ktrans * comp1.Lp * comp1.SV / meanKtrans');
model.component('comp1').physics('c').feature('cfeq1').set('f', 'comp1.Ktrans * comp1.Lp * comp1.SV * Peff / meanKtrans');
model.component('comp1').physics('c').feature('cfeq1').set('da', 0);

model.mesh('mpart1').run;

model.study.create('std1');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature.remove('fcDef');

model.result.create('pg1', 'PlotGroup3D');
model.result('pg1').create('slc1', 'Slice');

model.sol('sol1').attach('std1');
model.sol('sol1').runAll;

model.result('pg1').feature('slc1').set('quickplane', 'xy');
model.result('pg1').feature('slc1').set('quickznumber', 1);
model.result('pg1').feature('slc1').set('interactive', true);
model.result('pg1').feature('slc1').set('shift', -0.005);
model.result('pg1').feature('slc1').set('rangecoloractive', true);
model.result('pg1').feature('slc1').set('rangecolormax', 1500);
model.result('pg1').feature('slc1').set('resolution', 'normal');

% output parameter maps
% load([modelPath filesep 'roi_masks.mat'])
domain_roi = nii_load([modelPath filesep 'iso-cube_domain.nii.gz']);
dce_roi = nii_load([modelPath filesep 'dce-tumor_ROI.nii.gz']);

param = 'comp1.p';
param2 = 'comp1.KH*sqrt(comp1.px^2 + comp1.py^2)';

pmap = comsol2map(model,param,domain_roi);
pnii = domain_roi;
pnii.img = pmap;
nii_save(pnii,[modelPath filesep 'iso-comp1.p.nii.gz']);

pmapdce = interpCOMSOL(domain_roi,dce_roi,model,param);
pdcenii = dce_roi;
pdcenii.img = pmapdce;
nii_save(pdcenii,[modelPath filesep 'dce-comp1.p.nii.gz']);

vmap = comsol2map(model,param2,domain_roi);
vnii = domain_roi;
vnii.img = vmap;
nii_save(vnii,[modelPath filesep 'iso-comp1.v.nii.gz']);

vmapdce = interpCOMSOL(domain_roi,dce_roi,model,param2);
vdcenii = dce_roi;
vdcenii.img = vmapdce;
nii_save(vdcenii,[modelPath filesep 'dce-comp1.v.nii.gz']);

catch err
    err.message
    save('comsol_err.mat','err');
end

mphsave(model,[modelPath filesep modelLabel '.mph']);

