function model = comsol_model_init(modelPath,modelLabel)


import com.comsol.model.*
import com.comsol.model.util.*

model = ModelUtil.create('Model');

model.modelPath(modelPath);

model.label([modelLabel '.mph']);

model.comments(modelLabel);