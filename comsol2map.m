function pmap = comsol2map(model,param,domain_roi)

ROI = domain_roi.img;
[tx,ty,tz] = ind2sub(size(ROI),find(ROI));
pinterp = mphinterp(model,param,'coord',[tx';ty';tz']);
pmap = arr2map2(pinterp,ROI);
% save([modelPath 'pmap_comsol.mat'],'pmap');