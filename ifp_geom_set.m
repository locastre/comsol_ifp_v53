function model = ifp_geom_set(model,tumorSTL,domainSTL)


model.component('comp1').geom.create('geom1', 3);
model.component('comp1').geom('geom1').lengthUnit('mm');

model.component.create('mcomp1', 'MeshComponent');
model.geom.create('mgeom1', 3);
model.geom('mgeom1').lengthUnit('mm');

model.component.create('mcomp2', 'MeshComponent');
model.geom.create('mgeom2', 3);
model.geom('mgeom2').lengthUnit('mm');


model.component('comp1').mesh.create('mesh1');

model.mesh.create('mpart1', 'mgeom1');
model.mesh('mpart1').create('imp1', 'Import');
model.mesh('mpart1').feature('imp1').set('source', 'stlvrml');
model.mesh('mpart1').feature('imp1').set('filename', tumorSTL);
model.mesh('mpart1').run;

model.mesh.create('mpart2', 'mgeom2');
model.mesh('mpart2').create('imp1', 'Import');
model.mesh('mpart2').feature('imp1').set('source', 'stlvrml');
model.mesh('mpart2').feature('imp1').set('filename', domainSTL);
model.mesh('mpart2').run;



model.component('comp1').geom('geom1').create('imp1', 'Import');
model.component('comp1').geom('geom1').feature('imp1').label('Tumor');
model.component('comp1').geom('geom1').feature('imp1').set('type', 'mesh');
model.component('comp1').geom('geom1').feature('imp1').set('mesh', 'mpart1');


model.component('comp1').geom('geom1').create('imp2', 'Import');
model.component('comp1').geom('geom1').feature('imp2').label('Domain');
model.component('comp1').geom('geom1').feature('imp2').set('type', 'mesh');
model.component('comp1').geom('geom1').feature('imp2').set('mesh', 'mpart2');

model.component('comp1').geom('geom1').run;