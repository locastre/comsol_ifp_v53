comdir = '/home/eve/Comsol';

HNlist = deb(Dir2Arr(comdir,{'HN27*','HN28*'},'HN23*'));
wkN = {'week_0','week_2'};


for i = 1:size(HNlist,1)
	for j = 1:length(wkN)
		subjdir = deb(HNlist(i,:));
        [~,subj,~] = fileparts(subjdir);
		wk = wkN{j};
		modelPath = [subjdir filesep wk];
		modelLabel = [subj '_' wk '_v2'];
% 		m = comsol_ifp2(modelLabel,modelPath);
%         m = mphload([modelPath filesep modelLabel '.mph']);
        load([modelPath filesep 'roi_masks.mat']);
%         pmap = comsol2map(m,'comp1.p',domain_roi);
%         vmap = comsol2map(m,'K*sqrt(px^2 + py^2 + pz^2)',domain_roi);
%         save([modelPath filesep modelLabel '_comsolmaps.mat'],'pmap','vmap');
        load([modelPath filesep modelLabel '_comsolmaps.mat']);
        load([modelPath filesep 'meanKtrans.mat']);
        disp([modelLabel ' mean vals in ' num2str(length(find(tumor_roi))) 'mm^3 tumor: Ktrans ' num2str(meanKtransPerSec*60) 'per min, IFP ' num2str(mean(pmap(find(tumor_roi)))/1000) 'kPa, Vel ' num2str(mean(vmap(find(tumor_roi)))/1000) 'mm/s']);
	end
end

