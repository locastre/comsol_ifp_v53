function model = ifp_interp_import(model,Cptxt,Ktranstxt,vetxt)

model.component('comp1').func.create('int1', 'Interpolation');
model.component('comp1').func('int1').label('Cp');
model.component('comp1').func('int1').set('funcname', 'Cp');
model.component('comp1').func('int1').set('table', cellstr(string(load(Cptxt))));
model.component('comp1').func('int1').set('argunit', 's');
model.component('comp1').func('int1').set('fununit', 'mol');

model.file.create('res2');
model.file.create('res4');

model.component('comp1').func.create('int2', 'Interpolation');
model.component('comp1').func('int2').label('Ktrans');
model.component('comp1').func('int2').set('sourcetype', 'model');
model.component('comp1').func('int2').set('modelres', 'res2');
model.component('comp1').func('int2').set('importedname', 'Ktrans_persec.txt');
model.component('comp1').func('int2').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int2').set('importeddim', '3D');
model.component('comp1').func('int2').set('funcs', {'Ktrans' '1'});
model.component('comp1').func('int2').set('defvars', true);
model.component('comp1').func('int2').set('extrap', 'value');
model.component('comp1').func('int2').set('argunit', 'mm');
model.component('comp1').func('int2').set('fununit', '1/s');
model.component('comp1').func('int2').set('source', 'file');
model.component('comp1').func('int2').set('nargs', '3');
model.component('comp1').func('int2').set('struct', 'spreadsheet');
model.file('res2').resource(Ktranstxt);


model.component('comp1').func.create('int3', 'Interpolation');
model.component('comp1').func('int3').label('ve');
model.component('comp1').func('int3').set('sourcetype', 'model');
model.component('comp1').func('int3').set('modelres', 'res4');
model.component('comp1').func('int3').set('importedname', 've.txt');
model.component('comp1').func('int3').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int3').set('importeddim', '3D');
model.component('comp1').func('int3').set('funcs', {'ve' '1'});
model.component('comp1').func('int3').set('defvars', true);
model.component('comp1').func('int3').set('extrap', 'value');
model.component('comp1').func('int3').set('argunit', 'mm');
model.component('comp1').func('int3').set('source', 'file');
model.component('comp1').func('int3').set('nargs', '3');
model.component('comp1').func('int3').set('struct', 'spreadsheet');
model.file('res4').resource(vetxt);

