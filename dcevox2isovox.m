function [dceidx,isoidx] = dcevox2isovox(isonii,roinii)
%convert dcevox to coordinate space, find isospace vox

%DCE space header
H = [roinii.hdr.hist.srow_x;roinii.hdr.hist.srow_y;roinii.hdr.hist.srow_z;[0 0 0 1]];
% isospace header
S = [isonii.hdr.hist.srow_x;isonii.hdr.hist.srow_y;isonii.hdr.hist.srow_z;[0 0 0 1]];

ROI = roinii.img;
dceidx = find(ROI);

[i, j, k] = ind2sub(size(ROI),dceidx);

r = [(i-1)';(j-1)';(k-1)';ones(1,numel(i))];

x = zeros(size(r));
r0raw = zeros(size(r));
% r0 = zeros(size(r));

for n = 1:size(r,2)
    x(:,n) = H * r(:,n);
    r0raw(:,n) = round(S \ x(:,n));
%     r0(:,n) = round(S \ x(:,n));
end

r0 = 1+r0raw(1:3,:)';
% r0 = r0(1:3,:)';
isoidx = sub2ind(size(isonii.img),r0(:,1),r0(:,2),r0(:,3));