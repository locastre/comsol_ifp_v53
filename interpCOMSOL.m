function pmap = interpCOMSOL(pnii,roinii,hdr)

if nargin < 3
    hdr = roinii.hdr;
end

ROI = roinii.img;
% isoROI = pnii.img;

[dceidx,isoidx] = dcevox2isovox(pnii,roinii,hdr);
% pmap = interpCOMSOL(isodomainnii,roinii,model,param,dset)
% idxmatch = intersect(idx,find(isoROI));
% 
% [rx,ry,rz] = ind2sub(size(isoROI),idxmatch);
% 
% [rx,ry,rz] = ind2sub(size(isoROI),isoidx);
% r0match = [rx';ry';rz'];
% 
isopmap = pnii.img;
% pinterp = mphinterp(model,param,'coord',r0match,'dataset',dset);
pinterp = isopmap(isoidx);

pmap = zeros(size(ROI));

for j = 1:numel(dceidx)
    pmap(dceidx(j)) = pinterp(j);
end

