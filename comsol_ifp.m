function model = comsol_ifp(modelLabel,modelPath) %,smoothOpt) %,meanKtransmat,tumorSTL,domainSTL,Ktranstxt,vetxt,Cptxt)
%
% HN278D_wk0.m
%
% Model exported on Feb 19 2018, 14:04 by COMSOL 5.3.0.260.

import com.comsol.model.*
import com.comsol.model.util.*

meanKtransmat = [modelPath filesep 'meanKtrans.mat'];
% if smoothOpt
%     tumorSTL = [modelPath filesep 'iso-tumor_ROI_smooth.stl'];
% else
tumorSTL = [modelPath filesep 'iso-tumor_ROI.stl'];
% end
domainSTL = [modelPath filesep 'iso-domain_ROI.stl'];
Ktranstxt = [modelPath filesep 'Ktrans_persec.txt'];
vetxt = [modelPath filesep 've.txt'];
Cptxt = [modelPath filesep 'Ct_aif.txt'];

load(meanKtransmat);

model = ModelUtil.create('Model');

% model.modelPath('/mnt/freenas/Comsol/HN278D/week_0');
model.modelPath(modelPath);

model.label(modelLabel);

model.comments([modelLabel '\n']);
try
model.param.set('Pv', '2300 [Pa]', 'Microvascular pressure');
model.param.set('Deff', '1E-9 [m^2/s]', 'Effective coefficient of diffusion');
model.param.set('PIv', '2670 [Pa]', 'Osmotic pressure in microvasculature');
model.param.set('meanKtrans', [num2str(meanKtransPerSec) ' [1/s]'], 'mean Ktrans in tumor');
model.param.set('a1', '9.2 [kg/L]', 'fast decay');
model.param.set('m1', '0.005 [1/s]', 'fast decay');
model.param.set('a2', '4.2 [kg/L]', 'fast decay');
model.param.set('m2', '0.0008 [1/s]', 'fast decay');
model.param.set('d', '1 [mol/kg]');

model.component.create('comp1', true);

model.component('comp1').geom.create('geom1', 3);

model.component.create('mcomp1', 'MeshComponent');

model.geom.create('mgeom1', 3);

model.component.create('mcomp2', 'MeshComponent');

model.geom.create('mgeom2', 3);

model.file.create('res2');
model.file.create('res4');

model.result.table.create('evl3', 'Table');

model.component('comp1').func.create('int1', 'Interpolation');
model.component('comp1').func.create('int2', 'Interpolation');
model.component('comp1').func.create('int3', 'Interpolation');
model.component('comp1').func('int1').label('Cp');
model.component('comp1').func('int1').set('funcname', 'Cp');
model.component('comp1').func('int1').set('table', cellstr(string(load(Cptxt))));
model.component('comp1').func('int2').label('Ktrans');
model.component('comp1').func('int2').set('sourcetype', 'model');
model.component('comp1').func('int2').set('modelres', 'res2');
model.component('comp1').func('int2').set('importedname', 'Ktrans_persec.txt');
model.component('comp1').func('int2').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int2').set('importeddim', '3D');
model.component('comp1').func('int2').set('funcs', {'Ktrans' '1'});
model.component('comp1').func('int2').set('defvars', true);
model.component('comp1').func('int2').set('extrap', 'value');
model.component('comp1').func('int2').set('argunit', 'mm');
model.component('comp1').func('int2').set('fununit', '1/s');

model.file('res2').resource(Ktranstxt);

model.component('comp1').func('int2').set('source', 'file');
model.component('comp1').func('int2').set('nargs', '3');
model.component('comp1').func('int2').set('struct', 'spreadsheet');
model.component('comp1').func('int3').label('ve');
model.component('comp1').func('int3').set('sourcetype', 'model');
model.component('comp1').func('int3').set('modelres', 'res4');
model.component('comp1').func('int3').set('importedname', 've.txt');
model.component('comp1').func('int3').set('importedstruct', 'Spreadsheet');
model.component('comp1').func('int3').set('importeddim', '3D');
model.component('comp1').func('int3').set('funcs', {'ve' '1'});
model.component('comp1').func('int3').set('defvars', true);
model.component('comp1').func('int3').set('extrap', 'value');
model.component('comp1').func('int3').set('fununit', 'mm');

model.file('res4').resource(vetxt);

model.component('comp1').func('int3').set('source', 'file');
model.component('comp1').func('int3').set('nargs', '3');
model.component('comp1').func('int3').set('struct', 'spreadsheet');

model.component('comp1').mesh.create('mesh1');
model.mesh.create('mpart1', 'mgeom1');
model.mesh.create('mpart2', 'mgeom2');

model.geom('mgeom1').lengthUnit('mm');

model.mesh('mpart1').create('imp1', 'Import');
model.mesh('mpart2').create('imp1', 'Import');
model.mesh('mpart1').feature('imp1').set('source', 'stlvrml');
model.mesh('mpart1').feature('imp1').set('filename', tumorSTL);
model.mesh('mpart1').run;

model.geom('mgeom2').lengthUnit('mm');

model.mesh('mpart2').feature('imp1').set('source', 'stlvrml');
model.mesh('mpart2').feature('imp1').set('filename', domainSTL);
model.mesh('mpart2').run;

model.component('comp1').geom('geom1').lengthUnit('mm');
model.component('comp1').geom('geom1').create('imp1', 'Import');
model.component('comp1').geom('geom1').feature('imp1').label('Tumor');
model.component('comp1').geom('geom1').feature('imp1').set('type', 'mesh');
model.component('comp1').geom('geom1').feature('imp1').set('mesh', 'mpart1');
model.component('comp1').geom('geom1').create('imp2', 'Import');
model.component('comp1').geom('geom1').feature('imp2').label('Domain');
model.component('comp1').geom('geom1').feature('imp2').set('type', 'mesh');
model.component('comp1').geom('geom1').feature('imp2').set('mesh', 'mpart2');
model.component('comp1').geom('geom1').run;

model.component('comp1').variable.create('var1');
model.component('comp1').variable('var1').set('PIi', '1330 [Pa]', 'Osmotic pressure in interstitum');
model.component('comp1').variable('var1').set('SIGMAt', '0.91', 'Averrage osmotic reflefction coeff');
model.component('comp1').variable('var1').set('SV', '7000 [1/m]', 'Microvascular surface area crossection');
model.component('comp1').variable('var1').set('Lp', '3e-12 [m/Pa/s]', 'Vessel permeability');
model.component('comp1').variable('var1').set('K', '3.8e-13 [m*m/Pa/s]', 'Hydraulic conductivity');
model.component('comp1').variable('var1').set('eta', '1e-7 [1/Pa/s]', 'Lymphatic filtration coeff');
model.component('comp1').variable('var1').set('pL', '0 [Pa]');
model.component('comp1').variable('var1').set('phi', '0.0102', 'Mean v_e outside tumor');
model.component('comp1').variable('var1').selection.geom('geom1', 3);
model.component('comp1').variable('var1').selection.set([1]);
model.component('comp1').variable.create('var2');
model.component('comp1').variable('var2').set('PIi', '3230 [Pa]', 'Osmotic pressure in interstitum');
model.component('comp1').variable('var2').set('SIGMAt', '0.92', 'Average osmotic reflection coeff');
model.component('comp1').variable('var2').set('SV', '2e4 [1/m]', 'Microvascular surface area');
model.component('comp1').variable('var2').set('Lp', '2e-11 [m/Pa/s]', 'Vessel permeability');
model.component('comp1').variable('var2').set('K', '1.9e-12[m*m/Pa/s]', 'Hydraulic conductivity');
model.component('comp1').variable('var2').set('eta', '0 [1/Pa/s]', 'Lymphatic filtration coeff');
model.component('comp1').variable('var2').set('pL', '0 [Pa]', 'Lymphatic pressure in tissue');
model.component('comp1').variable('var2').set('phi', '0.0354', 'Mean v_e in tumor');
model.component('comp1').variable('var2').selection.geom('geom1', 3);
model.component('comp1').variable('var2').selection.set([2]);

model.view.create('view4', 3);

model.component('comp1').physics.create('c', 'CoefficientFormPDE', 'geom1');
model.component('comp1').physics('c').field('dimensionless').field('p');
model.component('comp1').physics('c').field('dimensionless').component({'p'});

model.result.table('evl3').label('Evaluation 3D');
model.result.table('evl3').comments('Interactive 3D values');

model.component('comp1').variable('var1').label('Normal Variables');
model.component('comp1').variable('var2').label('Tumor Variables');

model.component('comp1').physics('c').prop('Units').set('DependentVariableQuantity', 'pressure');
model.component('comp1').physics('c').prop('Units').set('CustomSourceTermUnit', '1/s');
model.component('comp1').physics('c').feature('cfeq1').set('c', {'K' '0' '0' '0' 'K' '0' '0' '0' 'K'});
model.component('comp1').physics('c').feature('cfeq1').set('a', '(comp1.Ktrans * Lp * SV / meanKtrans) + eta');
model.component('comp1').physics('c').feature('cfeq1').set('f', '(eta * pL) + (comp1.Ktrans * Lp * SV * (Pv - SIGMAt * (PIv - PIi)) / meanKtrans)');
model.component('comp1').physics('c').feature('cfeq1').label('IFP');

model.mesh('mpart1').run;
model.mesh('mpart2').run;

model.study.create('std1');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature.remove('fcDef');

model.result.dataset.create('int2_ds1', 'Grid3D');
model.result.dataset.create('int2_ds2', 'Grid3D');
model.result.dataset('int2_ds1').set('data', 'none');
model.result.dataset('int2_ds2').set('data', 'none');
model.result.create('pg1', 'PlotGroup3D');
model.result.create('pg2', 'PlotGroup3D');
model.result('pg1').create('plot1', 'Slice');
model.result('pg2').create('slc1', 'Slice');

model.sol('sol1').attach('std1');
model.sol('sol1').runAll;

model.result.dataset('int2_ds1').set('function', 'int2');
model.result.dataset('int2_ds1').set('parmin1', 136);
model.result.dataset('int2_ds1').set('parmax1', 196);
model.result.dataset('int2_ds1').set('parmin2', 77);
model.result.dataset('int2_ds1').set('parmax2', 151);
model.result.dataset('int2_ds1').set('parmin3', 17);
model.result.dataset('int2_ds1').set('parmax3', 76);
model.result.dataset('int2_ds2').set('function', 'int2');
model.result.dataset('int2_ds2').set('parmin1', 136);
model.result.dataset('int2_ds2').set('parmax1', 196);
model.result.dataset('int2_ds2').set('parmin2', 77);
model.result.dataset('int2_ds2').set('parmax2', 151);
model.result.dataset('int2_ds2').set('parmin3', 17);
model.result.dataset('int2_ds2').set('parmax3', 76);
model.result('pg1').label('Ktrans');
model.result('pg1').set('data', 'none');
model.result('pg1').set('titletype', 'manual');
model.result('pg1').set('title', 'Ktrans(x,y,z) (1/s)');
model.result('pg1').set('edges', false);
model.result('pg1').feature('plot1').set('data', 'int2_ds2');
model.result('pg1').feature('plot1').set('solrepresentation', 'solnum');
model.result('pg1').feature('plot1').set('expr', 'comp1.Ktrans(root.x[mm],root.y[mm],root.z[mm])');
model.result('pg1').feature('plot1').set('unit', '');
model.result('pg1').feature('plot1').set('descr', 'Ktrans(x,y,z)');
model.result('pg1').feature('plot1').set('quickplane', 'xy');
model.result('pg1').feature('plot1').set('quickznumber', 3);
model.result('pg1').feature('plot1').set('interactive', true);
model.result('pg1').feature('plot1').set('shift', -32);
model.result('pg1').feature('plot1').set('rangecoloractive', true);
model.result('pg1').feature('plot1').set('rangecolormax', 0.016);
model.result('pg1').feature('plot1').set('smooth', 'none');
model.result('pg1').feature('plot1').set('allowmaterialsmoothing', false);
model.result('pg1').feature('plot1').set('resolution', 'normal');
model.result('pg2').label('IFP');
model.result('pg2').feature('slc1').set('titletype', 'manual');
model.result('pg2').feature('slc1').set('title', [modelLabel ': Interstitial Fluid Pressure (Pa)']);
model.result('pg2').feature('slc1').set('quickplane', 'xy');
model.result('pg2').feature('slc1').set('quickznumber', 1);
model.result('pg2').feature('slc1').set('interactive', true);
model.result('pg2').feature('slc1').set('shift', -0.01);
model.result('pg2').feature('slc1').set('resolution', 'normal');
catch err
    err.message
end
mphsave(model,[modelPath filesep modelLabel '.mph'])
