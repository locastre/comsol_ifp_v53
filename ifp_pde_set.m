function model = ifp_pde_set(model)


model.component('comp1').physics.create('c', 'CoefficientFormPDE', 'geom1');
model.component('comp1').physics('c').field('dimensionless').field('p');
model.component('comp1').physics('c').field('dimensionless').component({'p'});


model.component('comp1').physics('c').label('Stationary IFP');
model.component('comp1').physics('c').prop('Units').set('DependentVariableQuantity', 'pressure');
model.component('comp1').physics('c').prop('Units').set('CustomSourceTermUnit', '1/s');
model.component('comp1').physics('c').feature('cfeq1').set('c', {'K' '0' '0' '0' 'K' '0' '0' '0' 'K'});
model.component('comp1').physics('c').feature('cfeq1').set('a', '(comp1.Ktrans * Lp * SV / meanKtrans) + eta');
model.component('comp1').physics('c').feature('cfeq1').set('f', '(eta * pL) + (comp1.Ktrans * Lp * SV * (Pv - SIGMAt * (PIv - PIi)) / meanKtrans)');
model.component('comp1').physics('c').feature('cfeq1').label('IFP');