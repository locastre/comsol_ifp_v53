function [meanKtransPerSec,tumorSTL,domainSTL,Ktranstxt,vetxt,Cptxt,roimat,screencapPath] = ifp_load_mats(modelPath)


%load files
meanKtransmat = [modelPath filesep 'meanKtrans.mat'];
tumorSTL = [modelPath filesep 'iso-tumor_ROI.stl'];
domainSTL = [modelPath filesep 'iso-domain_ROI.stl'];
Ktranstxt = [modelPath filesep 'Ktrans_persec.txt'];
vetxt = [modelPath filesep 've.txt'];
Cptxt = [modelPath filesep 'Ct_aif.txt'];
roimat = [modelPath filesep 'roi_masks.mat'];

screencapPath = [modelPath filesep 'screencaptures'];

if ~exist(screencapPath,'dir')
    mkdir(screencapPath);
end

% load meanKtranspersec variable
load(meanKtransmat,'meanKtransPerSec');