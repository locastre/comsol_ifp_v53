function model = ifp_study_stationary_add(model)



model.study.create('std1');
model.study('std1').create('stat', 'Stationary');
model.study('std1').label('Stationary IFP');
    
model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').feature.remove('fcDef');

model.sol('sol1').attach('std1');