function model = ifp_param_set(model)


%global parameters
model.param.set('Pv', '2300 [Pa]', 'Microvascular pressure');
model.param.set('Deff', '1E-9 [m^2/s]', 'Effective coefficient of diffusion');
model.param.set('PIv', '2670 [Pa]', 'Osmotic pressure in microvasculature');
model.param.set('meanKtrans', '0.0014112 [1/s]', 'mean Ktrans in tumor');
model.param.set('a1', '9.2 [kg/L]', 'fast decay');
model.param.set('m1', '0.005 [1/s]', 'fast decay');
model.param.set('a2', '4.2 [kg/L]', 'fast decay');
model.param.set('m2', '0.0008 [1/s]', 'fast decay');
model.param.set('d', '1 [mol/kg]');

%normal tissue variables


%tumor tissue variables

