function model = ifp_var_set(model)


model.component('comp1').variable.create('var1');
model.component('comp1').variable('var1').set('PIi', '1330 [Pa]', 'Osmotic pressure in interstitum');
model.component('comp1').variable('var1').set('SIGMAt', '0.91', 'Averrage osmotic reflefction coeff');
model.component('comp1').variable('var1').set('SV', '7000 [1/m]', 'Microvascular surface area crossection');
model.component('comp1').variable('var1').set('Lp', '3e-12 [m/Pa/s]', 'Vessel permeability');
model.component('comp1').variable('var1').set('K', '3.8e-13 [m*m/Pa/s]', 'Hydraulic conductivity');
model.component('comp1').variable('var1').set('eta', '1e-7 [1/Pa/s]', 'Lymphatic filtration coeff');
model.component('comp1').variable('var1').set('pL', '0 [Pa]');
model.component('comp1').variable('var1').set('phi', '0.0102', 'Mean v_e outside tumor');
% model.component('comp1').variable('var1').selection.geom('geom1', 3);
% model.component('comp1').variable('var1').selection.set([1]);

model.component('comp1').variable.create('var2');
model.component('comp1').variable('var2').set('PIi', '3230 [Pa]', 'Osmotic pressure in interstitum');
model.component('comp1').variable('var2').set('SIGMAt', '0.92', 'Average osmotic reflection coeff');
model.component('comp1').variable('var2').set('SV', '2e4 [1/m]', 'Microvascular surface area');
model.component('comp1').variable('var2').set('Lp', '2e-11 [m/Pa/s]', 'Vessel permeability');
model.component('comp1').variable('var2').set('K', '1.9e-12[m*m/Pa/s]', 'Hydraulic conductivity');
model.component('comp1').variable('var2').set('eta', '0 [1/Pa/s]', 'Lymphatic filtration coeff');
model.component('comp1').variable('var2').set('pL', '0 [Pa]', 'Lymphatic pressure in tissue');
model.component('comp1').variable('var2').set('phi', '0.0354', 'Mean v_e in tumor');
% model.component('comp1').variable('var2').selection.geom('geom1', 3);
% model.component('comp1').variable('var2').selection.set([2]);


model.component('comp1').variable('var1').label('Normal Variables');
model.component('comp1').variable('var2').label('Tumor Variables');