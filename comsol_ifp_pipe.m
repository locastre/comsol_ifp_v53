function model = comsol_ifp_pipe(modelLabel,modelPath,bmetsflag) 
%
% HNN277D_week_0_IFP.m
%
% Model exported on Feb 27 2018, 15:26 by COMSOL 5.3.0.260.

import com.comsol.model.*
import com.comsol.model.util.*

if nargin < 3
    bmetsflag = 0;
end

%load files
[meanKtransPerSec,tumorSTL,domainSTL,Ktranstxt,vetxt,Cptxt,roimat,screencapPath] = ifp_load_mats(modelPath);

% meanKtransmat = [modelPath filesep 'meanKtrans.mat'];
% tumorSTL = [modelPath filesep 'iso-tumor_ROI.stl'];
% domainSTL = [modelPath filesep 'iso-domain_ROI.stl'];
% Ktranstxt = [modelPath filesep 'Ktrans_persec.txt'];
% vetxt = [modelPath filesep 've.txt'];
% Cptxt = [modelPath filesep 'Ct_aif.txt'];
% roimat = [modelPath filesep 'roi_masks.mat'];
% 
% screencapPath = [modelPath filesep 'screencaptures'];
% 
% if ~exist(screencapPath,'dir')
%     mkdir(screencapPath);
% end
% 
% load(meanKtransmat);
disp(['Ktrans mean in tumor for ' modelLabel ' = ' num2str(meanKtransPerSec)]);

model = comsol_model_init(modelPath,modelLabel);

% 
% model = ModelUtil.create('Model');
% 
% model.modelPath(modelPath);
% 
% model.label([modelLabel '.mph']);
% 
% model.comments(modelLabel);

try
%     model.param.set('Pv', '2300 [Pa]', 'Microvascular pressure');
%     model.param.set('Deff', '1E-9 [m^2/s]', 'Effective coefficient of diffusion');
%     model.param.set('PIv', '2670 [Pa]', 'Osmotic pressure in microvasculature');
%     model.param.set('meanKtrans', '0.0014112 [1/s]', 'mean Ktrans in tumor');
%     model.param.set('a1', '9.2 [kg/L]', 'fast decay');
%     model.param.set('m1', '0.005 [1/s]', 'fast decay');
%     model.param.set('a2', '4.2 [kg/L]', 'fast decay');
%     model.param.set('m2', '0.0008 [1/s]', 'fast decay');
%     model.param.set('d', '1 [mol/kg]');

    model = ifp_param_set(model);
    
    model.component.create('comp1', true);
%     
%     model.component('comp1').geom.create('geom1', 3);
%     
%     model.component.create('mcomp1', 'MeshComponent');
%     
%     model.geom.create('mgeom1', 3);
%     
%     model.component.create('mcomp2', 'MeshComponent');
%     
%     model.geom.create('mgeom2', 3);
    
%     model.file.create('res2');
%     model.file.create('res4');
    
    model.result.table.create('evl3', 'Table');
    
    if bmetsflag
        model = ifp_var_set_bmets(model);
    else
        model = ifp_var_set(model);
    end
    
    model = ifp_interp_import(model,Cptxt,Ktranstxt,vetxt);
    
    model = ifp_geom_set(model,tumorSTL,domainSTL);
    
%     model.component('comp1').func.create('int1', 'Interpolation');
%     model.component('comp1').func.create('int2', 'Interpolation');
%     model.component('comp1').func.create('int3', 'Interpolation');
%     model.component('comp1').func('int1').label('Cp');
%     model.component('comp1').func('int1').set('funcname', 'Cp');
%     model.component('comp1').func('int1').set('table', cellstr(string(load(Cptxt))));
%     model.component('comp1').func('int1').set('argunit', 's');
%     model.component('comp1').func('int1').set('fununit', 'mol');
%     model.component('comp1').func('int2').label('Ktrans');
%     model.component('comp1').func('int2').set('sourcetype', 'model');
%     model.component('comp1').func('int2').set('modelres', 'res2');
%     model.component('comp1').func('int2').set('importedname', 'Ktrans_persec.txt');
%     model.component('comp1').func('int2').set('importedstruct', 'Spreadsheet');
%     model.component('comp1').func('int2').set('importeddim', '3D');
%     model.component('comp1').func('int2').set('funcs', {'Ktrans' '1'});
%     model.component('comp1').func('int2').set('defvars', true);
%     model.component('comp1').func('int2').set('extrap', 'value');
%     model.component('comp1').func('int2').set('argunit', 'mm');
%     model.component('comp1').func('int2').set('fununit', '1/s');
    
%     model.file('res2').resource(Ktranstxt);
%     
%     model.component('comp1').func('int2').set('source', 'file');
%     model.component('comp1').func('int2').set('nargs', '3');
%     model.component('comp1').func('int2').set('struct', 'spreadsheet');
%     model.component('comp1').func('int3').label('ve');
%     model.component('comp1').func('int3').set('sourcetype', 'model');
%     model.component('comp1').func('int3').set('modelres', 'res4');
%     model.component('comp1').func('int3').set('importedname', 've.txt');
%     model.component('comp1').func('int3').set('importedstruct', 'Spreadsheet');
%     model.component('comp1').func('int3').set('importeddim', '3D');
%     model.component('comp1').func('int3').set('funcs', {'ve' '1'});
%     model.component('comp1').func('int3').set('defvars', true);
%     model.component('comp1').func('int3').set('extrap', 'value');
%     model.component('comp1').func('int3').set('argunit', 'mm');
    
%     model.file('res4').resource(vetxt);
%     
%     model.component('comp1').func('int3').set('source', 'file');
%     model.component('comp1').func('int3').set('nargs', '3');
%     model.component('comp1').func('int3').set('struct', 'spreadsheet');
%     
%     model.component('comp1').mesh.create('mesh1');
%     model.mesh.create('mpart1', 'mgeom1');
%     model.mesh.create('mpart2', 'mgeom2');
%     
%     model.geom('mgeom1').lengthUnit('mm');
%     
%     model.mesh('mpart1').create('imp1', 'Import');
%     model.mesh('mpart2').create('imp1', 'Import');
%     model.mesh('mpart1').feature('imp1').set('source', 'stlvrml');
%     model.mesh('mpart1').feature('imp1').set('filename', tumorSTL);
%     model.mesh('mpart1').run;
%     
%     model.geom('mgeom2').lengthUnit('mm');
%     
%     model.mesh('mpart2').feature('imp1').set('source', 'stlvrml');
%     model.mesh('mpart2').feature('imp1').set('filename', domainSTL);
%     model.mesh('mpart2').run;
%     
%     model.component('comp1').geom('geom1').lengthUnit('mm');
%     model.component('comp1').geom('geom1').create('imp1', 'Import');
%     model.component('comp1').geom('geom1').feature('imp1').label('Tumor');
%     model.component('comp1').geom('geom1').feature('imp1').set('type', 'mesh');
%     model.component('comp1').geom('geom1').feature('imp1').set('mesh', 'mpart1');
%     model.component('comp1').geom('geom1').create('imp2', 'Import');
%     model.component('comp1').geom('geom1').feature('imp2').label('Domain');
%     model.component('comp1').geom('geom1').feature('imp2').set('type', 'mesh');
%     model.component('comp1').geom('geom1').feature('imp2').set('mesh', 'mpart2');
%     model.component('comp1').geom('geom1').run;
%     
%     model.component('comp1').variable.create('var1');
%     model.component('comp1').variable('var1').set('PIi', '1330 [Pa]', 'Osmotic pressure in interstitum');
%     model.component('comp1').variable('var1').set('SIGMAt', '0.91', 'Averrage osmotic reflefction coeff');
%     model.component('comp1').variable('var1').set('SV', '7000 [1/m]', 'Microvascular surface area crossection');
%     model.component('comp1').variable('var1').set('Lp', '3e-12 [m/Pa/s]', 'Vessel permeability');
%     model.component('comp1').variable('var1').set('K', '3.8e-13 [m*m/Pa/s]', 'Hydraulic conductivity');
%     model.component('comp1').variable('var1').set('eta', '1e-7 [1/Pa/s]', 'Lymphatic filtration coeff');
%     model.component('comp1').variable('var1').set('pL', '0 [Pa]');
%     model.component('comp1').variable('var1').set('phi', '0.0102', 'Mean v_e outside tumor');
    model.component('comp1').variable('var1').selection.geom('geom1', 3);
    model.component('comp1').variable('var1').selection.set([1]);
%     model.component('comp1').variable.create('var2');
%     model.component('comp1').variable('var2').set('PIi', '3230 [Pa]', 'Osmotic pressure in interstitum');
%     model.component('comp1').variable('var2').set('SIGMAt', '0.92', 'Average osmotic reflection coeff');
%     model.component('comp1').variable('var2').set('SV', '2e4 [1/m]', 'Microvascular surface area');
%     model.component('comp1').variable('var2').set('Lp', '2e-11 [m/Pa/s]', 'Vessel permeability');
%     model.component('comp1').variable('var2').set('K', '1.9e-12[m*m/Pa/s]', 'Hydraulic conductivity');
%     model.component('comp1').variable('var2').set('eta', '0 [1/Pa/s]', 'Lymphatic filtration coeff');
%     model.component('comp1').variable('var2').set('pL', '0 [Pa]', 'Lymphatic pressure in tissue');
%     model.component('comp1').variable('var2').set('phi', '0.0354', 'Mean v_e in tumor');
    model.component('comp1').variable('var2').selection.geom('geom1', 3);
    model.component('comp1').variable('var2').selection.set([2]);
    
    model.view.create('view4', 3);
    model.view.create('view5', 3);
%     
%     model.component('comp1').physics.create('c', 'CoefficientFormPDE', 'geom1');
%     model.component('comp1').physics('c').field('dimensionless').field('p');
%     model.component('comp1').physics('c').field('dimensionless').component({'p'});
    
    model.result.table('evl3').label('Evaluation 3D');
    model.result.table('evl3').comments('Interactive 3D values');
%     
%     model.component('comp1').variable('var1').label('Normal Variables');
%     model.component('comp1').variable('var2').label('Tumor Variables');
%     
%     model.component('comp1').physics('c').label('Stationary IFP');
%     model.component('comp1').physics('c').prop('Units').set('DependentVariableQuantity', 'pressure');
%     model.component('comp1').physics('c').prop('Units').set('CustomSourceTermUnit', '1/s');
%     model.component('comp1').physics('c').feature('cfeq1').set('c', {'K' '0' '0' '0' 'K' '0' '0' '0' 'K'});
%     model.component('comp1').physics('c').feature('cfeq1').set('a', '(comp1.Ktrans * Lp * SV / meanKtrans) + eta');
%     model.component('comp1').physics('c').feature('cfeq1').set('f', '(eta * pL) + (comp1.Ktrans * Lp * SV * (Pv - SIGMAt * (PIv - PIi)) / meanKtrans)');
%     model.component('comp1').physics('c').feature('cfeq1').label('IFP');
%     
%     model.mesh('mpart1').run;
%     model.mesh('mpart2').run;
%     
%     model.study.create('std1');
%     model.study('std1').create('stat', 'Stationary');
%     
%     model.sol.create('sol1');
%     model.sol('sol1').study('std1');
%     model.sol('sol1').attach('std1');
%     model.sol('sol1').create('st1', 'StudyStep');
%     model.sol('sol1').create('v1', 'Variables');
%     model.sol('sol1').create('s1', 'Stationary');
%     model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
%     model.sol('sol1').feature('s1').feature.remove('fcDef');

    model = ifp_pde_set(model);
    model = ifp_study_stationary_add(model);
    
    model.result.dataset.create('int2_ds1', 'Grid3D');
    model.result.dataset.create('int2_ds2', 'Grid3D');
    model.result.dataset.create('int1_ds1', 'Grid1D');
    model.result.dataset.create('ptds1', 'CutPoint1D');
    model.result.dataset.create('int1_ds2', 'Grid1D');
    model.result.dataset.create('int1_ds3', 'Grid1D');
    model.result.dataset('int2_ds1').set('data', 'none');
    model.result.dataset('int2_ds2').set('data', 'none');
    model.result.dataset('int1_ds1').set('data', 'none');
    model.result.dataset('int1_ds2').set('data', 'none');
    model.result.dataset('int1_ds3').set('data', 'none');
    model.result.create('pg2', 'PlotGroup3D');
    model.result.create('pg3', 'PlotGroup1D');
    model.result.create('pg5', 'PlotGroup3D');
    model.result('pg2').create('slc1', 'Slice');
    model.result('pg3').create('plot1', 'LineGraph');
    model.result('pg3').create('ptplot1', 'PointGraph');
    model.result('pg3').create('plot2', 'LineGraph');
    model.result('pg3').create('plot3', 'LineGraph');
    model.result('pg3').feature('plot1').set('xdata', 'expr');
    model.result('pg3').feature('plot2').set('xdata', 'expr');
    model.result('pg3').feature('plot3').set('xdata', 'expr');
    model.result('pg5').create('slc1', 'Slice');
    model.result.export.create('img1', 'Image3D');
    model.result.export.create('img2', 'Image1D');
    model.result.export.create('img3', 'Image3D');
    
    model.study('std1').label('Stationary IFP');
    
    model.sol('sol1').attach('std1');
    model.sol('sol1').runAll;
    
    model.result.dataset('int2_ds1').set('function', 'int2');
    model.result.dataset('int2_ds1').set('parmin1', 136);
    model.result.dataset('int2_ds1').set('parmax1', 196);
    model.result.dataset('int2_ds1').set('parmin2', 77);
    model.result.dataset('int2_ds1').set('parmax2', 151);
    model.result.dataset('int2_ds1').set('parmin3', 17);
    model.result.dataset('int2_ds1').set('parmax3', 76);
    model.result.dataset('int2_ds2').set('function', 'int2');
    model.result.dataset('int2_ds2').set('parmin1', 136);
    model.result.dataset('int2_ds2').set('parmax1', 196);
    model.result.dataset('int2_ds2').set('parmin2', 77);
    model.result.dataset('int2_ds2').set('parmax2', 151);
    model.result.dataset('int2_ds2').set('parmin3', 17);
    model.result.dataset('int2_ds2').set('parmax3', 76);
    model.result.dataset('int1_ds1').set('function', 'int1');
    model.result.dataset('int1_ds1').set('par1', 't');
    model.result.dataset('int1_ds1').set('parmax1', 296.1);
    model.result.dataset('ptds1').set('pointx', '0.0 7.6 15.2 22.8 30.4 38.0 45.5 53.1 60.7 68.3 75.9 83.5 91.1 98.7 106.3 113.9 121.5 129.1 136.6 144.2 151.8 159.4 167.0 174.6 182.2 189.8 197.4 205.0 212.6 220.2 227.7 235.3 242.9 250.5 258.1 265.7 273.3 280.9 288.5 296.1 ');
    model.result.dataset('int1_ds2').set('function', 'int1');
    model.result.dataset('int1_ds2').set('par1', 't');
    model.result.dataset('int1_ds2').set('parmin1', -29.610000000000003);
    model.result.dataset('int1_ds2').set('parmax1', 0);
    model.result.dataset('int1_ds3').set('function', 'int1');
    model.result.dataset('int1_ds3').set('par1', 't');
    model.result.dataset('int1_ds3').set('parmin1', 296.1);
    model.result.dataset('int1_ds3').set('parmax1', 325.71000000000004);
    model.result('pg2').label('IFP');
    model.result('pg2').set('view', 'view1');
    model.result('pg2').feature('slc1').set('titletype', 'manual');
    model.result('pg2').feature('slc1').set('title', [modelLabel ': Interstitial Fluid Pressure (Pa)']);
    model.result('pg2').feature('slc1').set('quickplane', 'xy');
    model.result('pg2').feature('slc1').set('quickznumber', '1.0');
    model.result('pg2').feature('slc1').set('interactive', true);
    model.result('pg2').feature('slc1').set('shift', -0.011);
    model.result('pg2').feature('slc1').set('resolution', 'normal');
    model.result('pg3').label('Cp(t)');
    model.result('pg3').set('data', 'none');
    model.result('pg3').set('titletype', 'manual');
    model.result('pg3').set('title', 'Cp(t)');
    model.result('pg3').set('xlabelactive', true);
    model.result('pg3').set('ylabelactive', true);
    model.result('pg3').feature('plot1').set('data', 'int1_ds1');
    model.result('pg3').feature('plot1').set('solrepresentation', 'solnum');
    model.result('pg3').feature('plot1').set('expr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('plot1').set('unit', '');
    model.result('pg3').feature('plot1').set('descr', 'Cp(t)');
    model.result('pg3').feature('plot1').set('xdataexpr', 'root.t');
    model.result('pg3').feature('plot1').set('xdataunit', '');
    model.result('pg3').feature('plot1').set('xdatadescr', 'root.t');
    model.result('pg3').feature('plot1').set('allowmaterialsmoothing', false);
    model.result('pg3').feature('plot1').set('resolution', 'normal');
    model.result('pg3').feature('ptplot1').set('data', 'ptds1');
    model.result('pg3').feature('ptplot1').set('solrepresentation', 'solnum');
    model.result('pg3').feature('ptplot1').set('expr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('ptplot1').set('unit', '');
    model.result('pg3').feature('ptplot1').set('descr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('ptplot1').set('xdata', 'expr');
    model.result('pg3').feature('ptplot1').set('xdataexpr', 'root.t');
    model.result('pg3').feature('ptplot1').set('xdataunit', '');
    model.result('pg3').feature('ptplot1').set('xdatadescr', 'root.t');
    model.result('pg3').feature('ptplot1').set('linecolor', 'black');
    model.result('pg3').feature('plot2').label('Left Extrapolation');
    model.result('pg3').feature('plot2').set('data', 'int1_ds2');
    model.result('pg3').feature('plot2').set('solrepresentation', 'solnum');
    model.result('pg3').feature('plot2').set('expr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('plot2').set('unit', '');
    model.result('pg3').feature('plot2').set('descr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('plot2').set('xdataexpr', 'root.t');
    model.result('pg3').feature('plot2').set('xdataunit', '');
    model.result('pg3').feature('plot2').set('xdatadescr', 'root.t');
    model.result('pg3').feature('plot2').set('linestyle', 'dashed');
    model.result('pg3').feature('plot2').set('linecolor', 'red');
    model.result('pg3').feature('plot2').set('allowmaterialsmoothing', false);
    model.result('pg3').feature('plot2').set('resolution', 'normal');
    model.result('pg3').feature('plot3').label('Right Extrapolation');
    model.result('pg3').feature('plot3').set('data', 'int1_ds3');
    model.result('pg3').feature('plot3').set('solrepresentation', 'solnum');
    model.result('pg3').feature('plot3').set('expr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('plot3').set('unit', '');
    model.result('pg3').feature('plot3').set('descr', 'comp1.Cp(root.t)');
    model.result('pg3').feature('plot3').set('xdataexpr', 'root.t');
    model.result('pg3').feature('plot3').set('xdataunit', '');
    model.result('pg3').feature('plot3').set('xdatadescr', 'root.t');
    model.result('pg3').feature('plot3').set('linestyle', 'dashed');
    model.result('pg3').feature('plot3').set('linecolor', 'red');
    model.result('pg3').feature('plot3').set('allowmaterialsmoothing', false);
    model.result('pg3').feature('plot3').set('resolution', 'normal');
    model.result('pg5').label('Steady State Fluid Velocity');
    model.result('pg5').set('view', 'view1');
    model.result('pg5').feature('slc1').set('expr', 'K*sqrt(px^2 + py^2 + pz^2)');
    model.result('pg5').feature('slc1').set('unit', 'mm/s');
    model.result('pg5').feature('slc1').set('descr', 'K*sqrt(px^2 + py^2 + pz^2)');
    model.result('pg5').feature('slc1').set('titletype', 'manual');
    model.result('pg5').feature('slc1').set('title', 'Fluid Velocity (m/s)');
    model.result('pg5').feature('slc1').set('quickplane', 'xy');
    model.result('pg5').feature('slc1').set('quickznumber', '1.0');
    model.result('pg5').feature('slc1').set('interactive', true);
    model.result('pg5').feature('slc1').set('shift', -0.011);
    model.result('pg5').feature('slc1').set('rangecoloractive', true);
    model.result('pg5').feature('slc1').set('rangecolormin', 1.9932195798054617E-9);
    model.result('pg5').feature('slc1').set('rangecolormax', '0.00075');
    model.result('pg5').feature('slc1').set('resolution', 'normal');
    model.result.export('img1').label('IFP');
    model.result.export('img1').set('view', 'view1');
    model.result.export('img1').set('pngfilename', [screencapPath filesep 'IFP']);
    model.result.export('img1').set('printunit', 'mm');
    model.result.export('img1').set('webunit', 'px');
    model.result.export('img1').set('printheight', '90');
    model.result.export('img1').set('webheight', '600');
    model.result.export('img1').set('printwidth', '120');
    model.result.export('img1').set('webwidth', '800');
    model.result.export('img1').set('printlockratio', 'off');
    model.result.export('img1').set('weblockratio', 'off');
    model.result.export('img1').set('printresolution', '300');
    model.result.export('img1').set('webresolution', '96');
    model.result.export('img1').set('size', 'current');
    model.result.export('img1').set('antialias', 'on');
    model.result.export('img1').set('zoomextents', 'off');
    model.result.export('img1').set('title', 'on');
    model.result.export('img1').set('legend', 'on');
    model.result.export('img1').set('logo', 'on');
    model.result.export('img1').set('options', 'on');
    model.result.export('img1').set('fontsize', '12');
    model.result.export('img1').set('customcolor', [1 1 1]);
    model.result.export('img1').set('background', 'color');
    model.result.export('img1').set('axisorientation', 'on');
    model.result.export('img1').set('grid', 'on');
    model.result.export('img1').set('qualitylevel', '92');
    model.result.export('img1').set('qualityactive', 'off');
    model.result.export('img1').set('imagetype', 'png');
    model.result.export('img2').label('Cp(t)');
    model.result.export('img2').set('pngfilename', [screencapPath filesep 'Cp']);
    model.result.export('img2').set('printunit', 'mm');
    model.result.export('img2').set('webunit', 'px');
    model.result.export('img2').set('printheight', '90');
    model.result.export('img2').set('webheight', '600');
    model.result.export('img2').set('printwidth', '120');
    model.result.export('img2').set('webwidth', '800');
    model.result.export('img2').set('printlockratio', 'off');
    model.result.export('img2').set('weblockratio', 'off');
    model.result.export('img2').set('printresolution', '300');
    model.result.export('img2').set('webresolution', '96');
    model.result.export('img2').set('size', 'current');
    model.result.export('img2').set('antialias', 'on');
    model.result.export('img2').set('zoomextents', 'off');
    model.result.export('img2').set('title', 'on');
    model.result.export('img2').set('legend', 'on');
    model.result.export('img2').set('logo', 'on');
    model.result.export('img2').set('options', 'on');
    model.result.export('img2').set('fontsize', '12');
    model.result.export('img2').set('customcolor', [1 1 1]);
    model.result.export('img2').set('background', 'color');
    model.result.export('img2').set('axes', 'on');
    model.result.export('img2').set('showgrid', 'on');
    model.result.export('img2').set('qualitylevel', '92');
    model.result.export('img2').set('qualityactive', 'off');
    model.result.export('img2').set('imagetype', 'png');
    model.result.export('img3').label('Steady-State Fluid Velocity');
    model.result.export('img3').set('plotgroup', 'pg5');
    model.result.export('img3').set('pngfilename', [screencapPath filesep 'ssfv']);
    model.result.export('img3').set('printunit', 'mm');
    model.result.export('img3').set('webunit', 'px');
    model.result.export('img3').set('printheight', '90');
    model.result.export('img3').set('webheight', '600');
    model.result.export('img3').set('printwidth', '120');
    model.result.export('img3').set('webwidth', '800');
    model.result.export('img3').set('printlockratio', 'off');
    model.result.export('img3').set('weblockratio', 'off');
    model.result.export('img3').set('printresolution', '300');
    model.result.export('img3').set('webresolution', '96');
    model.result.export('img3').set('size', 'current');
    model.result.export('img3').set('antialias', 'on');
    model.result.export('img3').set('zoomextents', 'off');
    model.result.export('img3').set('title', 'on');
    model.result.export('img3').set('legend', 'on');
    model.result.export('img3').set('logo', 'on');
    model.result.export('img3').set('options', 'on');
    model.result.export('img3').set('fontsize', '12');
    model.result.export('img3').set('customcolor', [1 1 1]);
    model.result.export('img3').set('background', 'color');
    model.result.export('img3').set('axisorientation', 'on');
    model.result.export('img3').set('grid', 'on');
    model.result.export('img3').set('qualitylevel', '92');
    model.result.export('img3').set('qualityactive', 'off');
    model.result.export('img3').set('imagetype', 'png');
    
    model.result.export('img1').run;
    model.result.export('img2').run;
    model.result.export('img3').run;
    
%     newmask = ones(size(domain_ROI));
%     [tx,ty,tz] = ind2sub(size(domain_ROI),find(domain_ROI));
%     pinterp = mphinterp(model,'comp1.p','coord',[tx';ty';tz']);
%     pmap = arr2map2(pinterp,newmask);
%     save([modelPath 'pmap_comsol.mat'],'pmap');
    
catch err
    err.message
end

mphsave(model,[modelPath filesep modelLabel '.mph']);